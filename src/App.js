import React, { useState } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import Swal from "sweetalert2";
import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/css";
import "swiper/css/effect-coverflow";
import "swiper/css/pagination";
import "./swiper.css";
import "swiper/css/navigation";

import {
  EffectCoverflow,
  Pagination,
  Autoplay,
  Navigation,
} from "swiper/modules";

function App() {
  const numeroDeTelefono = "4425475512"; // Reemplaza con el número de teléfono al que deseas enviar el mensaje

  // Hola buen día, soy [nombre invitado] asistiremos [número de personas que asistirán] persona(s) por parte de [la/el novio(a)] a la boda de Michelle & Francisco
  const [nombre, setNombre] = useState("");
  const [asistente, setAsistente] = useState();
  let mensaje = "";
  const handleNombreChange = (event) => {
    setNombre(event.target.value);
  };
  const handleAsistenteChange = (event) => {
    let input = event.target.value;
    input.replace(/[^0-9]/g, "");
    input = Math.min(Math.max(parseInt(input), 0), 10);
    setAsistente(input.toString());
  };

  const enviarWhatsApp = (dato) => {
    if (
      nombre === "" ||
      nombre === null ||
      nombre === undefined ||
      asistente === 0 ||
      asistente === null ||
      asistente === undefined
    ) {
      Swal.fire({
        icon: "warning",
        title: "Completa el formulario",
        showConfirmButton: false,
        timer: 2500,
      });
    } else {
      let textcomplent = "";
      if (dato === "novia") {
        textcomplent = "de la novia";
      } else {
        textcomplent = "del novio";
      }
      mensaje = ` Hola buen día, soy ${nombre} confirmo asistencia de ${asistente} persona(s) por parte ${textcomplent} a la boda de *Michelle & Francisco*`;
      const url = `https://api.whatsapp.com/send?phone=${numeroDeTelefono}&text=${encodeURIComponent(
        mensaje
      )}`;
      window.open(url, "_blank");
    }
  };

  const enviarIglesia = () => {
    window.open("https://maps.app.goo.gl/ds3LNRzDj5qUeUJS7", "_blank");
  };
  const enviarSalon = () => {
    window.open("https://maps.app.goo.gl/SohgazmmseW5p3n2A", "_blank");
  };
  return (
    <div id="parallax-world-of-ugg">
      <section className="section-title">
        <div className="title">
          <h3>Nuestra</h3>
          <h1>Boda</h1>
        </div>
      </section>

      <section>
        <div className="parallax-one">
          <h2 className="forzarcolor">Michelle & Francisco</h2>
        </div>
      </section>

      <section>
        <div className="block">
          <h4 className="center">Con la bendición de Dios y nuestros padres</h4>
          <div className="row">
            <div className="col">
              <p>Yesica Arellano Arteaga</p>
              <p>Ernesto Aguilar Arias</p>
            </div>
            <div className="col">
              <h4 className="center">&</h4>
            </div>
            <div className="col">
              <p>Graciela Bolaños de Paz</p>
              <p>Francisco Hernandez Castillo</p>
            </div>
          </div>

          <section>
            <div className="parallax-four"></div>
          </section>

          <h4 className="center">Acompañados de nuestros padrinos</h4>
          <div className="row">
            <div className="col">
              <p>José Guadalupe Juan Diaz Arteaga</p>
              <p>Elizabeth Lucas Mejía</p>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className="row block">
          <div className="col-12 col-sm-12 col-md-6 contenedor-respomsive-img">
            <img
              className="img-responsive img-shadow"
              src="./dist/img/p-05.jpg"
              alt="img1"
            />
          </div>
          <div className="col-12 col-sm-12 col-md-6 ">
            <h4 className="center">
              Tenemos el honor de invitarles a nuestra boda el
            </h4>
            <p className="forzarcolorfecha1">Viernes</p>
            <h4 className="forzarcolorfecha">22</h4>
            <p className="forzarcolorfecha1">Diciembre</p>
            <h4 className="forzarcolorfecha">2023</h4>
          </div>
        </div>
      </section>

      <section>
        <div className="block">
          <h4>Itinerario</h4>
        </div>
      </section>

      <div className="main-timeline-2 block">
        <div className="timeline-2 left-2 ">
          <div className="card">
            <img
              src="https://lh3.googleusercontent.com/p/AF1QipO5qignDt1K9jo2ExWw6zxRbS0JYWeCdwrtrFMq=s680-w680-h510"
              className="card-img-top"
              alt="Responsive image"
            />
            <div className="card-body">
              <h5>Ceremonia Religiosa</h5>
              <div className="parallax-six center"></div>
              <p className="text-muted">
                <i className="far fa-clock" aria-hidden="true"></i> 4:30 pm
              </p>
              <p className="fw-bold mb-2">
                Templo de Nuestra Señora del Carmen
              </p>
              <p className="mb-0">
                C. José Ma. Morelos, Centro, 76000 Santiago de Querétaro, Qro.
              </p>
              <div
                style={{
                  position: "relative",
                  marginBottom: "20%",
                  marginTop: "5%",
                }}
              >
                <button
                  onClick={enviarIglesia}
                  style={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                  type="button"
                  className="btn forzarcolorbtn mt-3"
                >
                  Ver mapa
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="timeline-2 right-2">
          <div className="card">
            <img
              src="https://lh3.googleusercontent.com/p/AF1QipNWTkWoTsrnaO7Pek-jpAn3CXLSknbrZ_ADveDL=s680-w680-h510"
              className="card-img-top"
              alt="Responsive image"
            />
            <div className="card-body">
              <h5>Recepción</h5>
              <div className="parallax-seven center"></div>
              <p className="text-muted">
                <i className="far fa-clock" aria-hidden="true"></i> 7:30 pm
              </p>
              <p className="fw-bold mb-2">Salón de Fiestas Villa Pueblito</p>
              <p className="mb-0">
                Heroico Colegio Militar 69, El Pueblito, 76900 El Pueblito, Qro.
              </p>
              <div
                style={{
                  position: "relative",
                  marginBottom: "20%",
                  marginTop: "5%",
                }}
              >
                <button
                  onClick={enviarSalon}
                  style={{
                    position: "absolute",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                  }}
                  type="button"
                  className="btn forzarcolorbtn mt-3"
                >
                  Ver mapa
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <section>
        <div className="block section-swiper">
          <div className="contenedor-swiper">
            <Swiper
              effect={"coverflow"}
              grabCursor={true}
              centeredSlides={true}
              slidesPerView={"auto"}
              coverflowEffect={{
                rotate: 50,
                stretch: 0,
                depth: 100,
                modifier: 1,
                slideShadows: true,
              }}
              // pagination={{
              //   clickable: true,
              // }}
              pagination={true}
              autoplay={{
                delay: 1500,
                disableOnInteraction: false,
              }}
              navigation={false}
              modules={[EffectCoverflow, Pagination, Autoplay, Navigation]}
              className="mySwiper"
            >
              {/*<SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/1.jpg" />
              </SwiperSlide>
              <SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/2.jpg" />
              </SwiperSlide>
               <SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/3.png" />
              </SwiperSlide> 
              <SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/4.jpg" />
              </SwiperSlide>
              <SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/5.jpg" />
              </SwiperSlide>*/}
              <SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/6.jpg" />
              </SwiperSlide>
              <SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/8.jpg" />
              </SwiperSlide>
              <SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/9.jpg" />
              </SwiperSlide>
              <SwiperSlide className="SwiperSlide">
                <img src="./dist/img/carrusel/10.jpg" />
              </SwiperSlide>
            </Swiper>
          </div>
        </div>
      </section>
      <section>
        <div className="block">
          <section>
            <div
              className="parallax-five"
              style={{ transform: "scaleX(-1)" }}
            ></div>
          </section>
          <h4>Mesa de regalos</h4>
          <div className="txtcenter mb-5">
            <img className="imgicono" src="./dist/img/image.png" alt="img1" />
            <div className="txtregalo mt-2 ">
              <h6>Sin ti esto no sería igual.</h6>
              <p>Gracias por tu compañía en esta nueva etapa que comenzamos.</p>
              <p>
                El regalo es opcional, la asistencia es obligatoria, pero si
                quieres traer un detalle agradecemos su muestra de afecto.
              </p>
            </div>
          </div>
          <h4>Código de vestimenta</h4>
          <div className="txtcenter mb-3">
            <img
              className="imgicono"
              src="./dist/img/dresscode.png"
              alt="img1"
            />
            <div className="txtregalo mt-2">
              <p>Formal</p>
            </div>
          </div>
          {/* <div className="row center">
            <button onClick={enviarWhatsApp} className="btn forzarcolorbtn mt-0">
              <p>Confirmar asistencia invitación</p>
            </button>
          </div> */}
        </div>
      </section>

      <section>
        <div className="parallax-three">
          <div className="card confirma">
            <div className="card-body">
              <h4>Confirma tu asistencia</h4>
              <p className="mb-4">
                Nuestro día tan anhelado será muy pronto y queremos celebrarlo
                con todas las personas que son importantes para nosotros, por lo
                cual será un gusto contar con su compañía.
              </p>
              <div>
                <form className="form-control bginput">
                  <div className="col-12">
                    <input
                      id="miid"
                      onChange={handleNombreChange}
                      value={nombre}
                      className="form-control inputsdesing"
                      placeholder="Nombre completo"
                      type="text"
                    />
                  </div>
                  <div className="col-12">
                    <input
                      id="miid"
                      onChange={handleAsistenteChange}
                      value={asistente}
                      className="form-control inputsdesing"
                      placeholder="Invitados"
                      type="number"
                      min="0"
                      max="10"
                    />
                    <small className="form-text text-muted">
                      Ingresa la cantidad de personas que asistirán contandote a
                      ti.
                    </small>
                  </div>
                </form>
              </div>
              <div className="row mb-4">
                <div className="col">
                  <h5>Por parte del novio</h5>
                  <button
                    type="button"
                    onClick={() => enviarWhatsApp("novio")}
                    className="btn forzarcolorbtn mt-0"
                  >
                    Confirmar
                  </button>
                </div>
                <div className="col">
                  <h5>Por parte de la novia</h5>
                  <button
                    type="button"
                    onClick={() => enviarWhatsApp("novia")}
                    className="btn forzarcolorbtn mt-0"
                  >
                    Confirmar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
