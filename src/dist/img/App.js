import { useEffect, useState } from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
function App() {
  const [isLoading, setIsLoading] = useState(true);
  useEffect(() => {
    setTimeout(() => {
      setIsLoading(false);
    }, 500);
  }, []);
  return (
    <div id="parallax-world-of-ugg">
      <section className="section-title">
        <div class="title">
          <h3>Nuestra</h3>
          <h1>Boda</h1>
        </div>
      </section>

      <section>
        <div class="parallax-one">
          <h2 className="forzarcolor">Michelle & Francisco</h2>
        </div>
      </section>

      <section>
        <div class="block">
          <h4 className="center">Padres</h4>
          <div className="row">
            <div className="col">
              <p>Yesica Arellano Arteaga a</p>
              <p>Ernesto Aguilar Arias</p>
            </div>

            <div className="col">
              <p>Graciela Bolaños de Paz</p>
              <p>Francisco Hernandez Castillo</p>
            </div>
          </div>

          <p class="line-break margin-top-10"></p>
          <h4 className="center">Padrinos</h4>
          <div className="row">
            <div className="col">
              <p>José Guadalupe Juan Diaz Arteaga</p>
              <p>Elizabeth Lucas Mejía</p>
            </div>
          </div>
          {/* <p class="margin-top-10"></p> */}
        </div>
      </section>

      <section>
        {/* <div class="parallax-two"><h2>NEW YORK</h2></div> */}
        <div className="block">
          <div className="row">
            <div className="col-9">
              <img className="imgtwo-section" src="./dist/img/4.png" />
            </div>
            <div className="col-3">texto del evento</div>
          </div>
        </div>
      </section>

      <section>
        <div class="block"></div>
      </section>

      {/* <section>
        <div class="parallax-three">
          <h2>ENCHANTED FOREST</h2>
        </div>
      </section> */}

      {/* <section>
        <div class="block">
          <p>
            <span class="first-character atw">W</span>hen the New York fashion
            community notices your brand, the world soon follows. The widespread
            love for UGG extended to Europe in the mid-2000's along with the
            stylish casual movement and demand for premium casual fashion. UGG
            boots and shoes were now seen walking the streets of London, Paris
            and Amsterdam with regularity. To meet the rising demand from new
            fans, UGG opened flagship stores in the UK and an additional
            location in Moscow. As the love spread farther East, concept stores
            were opened in Beijing, Shanghai and Tokyo. UGG Australia is now an
            international brand that is loved by all. This love is a result of a
            magical combination of the amazing functional benefits of sheepskin
            and the heightened emotional feeling you get when you slip them on
            your feet. In short, you just feel better all over when you wear UGG
            boots, slippers, and shoes.
          </p>
          <p class="line-break margin-top-10"></p>
          <p class="margin-top-10">
            In 2011, UGG will go back to its roots and focus on bringing the
            active men that brought the brand to life back with new styles
            allowing them to love the brand again as well. Partnering with Super
            Bowl champion and NFL MVP Tom Brady, UGG will invite even more men
            to feel the love the rest of the world knows so well. UGG will also
            step into the world of high fashion with UGG Collection. The UGG
            Collection fuses the timeless craft of Italian shoemaking with the
            reliable magic of sheepskin, bringing the luxurious feel of UGG to
            high end fashion. As the love for UGG continues to spread across the
            world, we have continued to offer new and unexpected ways to
            experience the brand. The UGG journey continues on and the love for
            UGG continues to spread.
          </p>
        </div>
      </section> */}
    </div>
  );
}

export default App;
